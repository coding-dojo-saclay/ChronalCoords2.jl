# Dojo

This is a solution for the Day 6 puzzle of "Advent of Code 2018", available
here: [https://adventofcode.com/2018/day/6](). Another Julia solution for the
same puzzle is proposed by Christian Poli here:
[https://gitlab.inria.fr/coding-dojo-saclay/ChronalCoords.jl]().

## Installation & setup

```
bash> git clone https://gitlab.inria.fr/coding-dojo-saclay/ChronalCoords2.jl
Cloning into 'ChronalCoords2.jl'...
remote: Enumerating objects: 18, done.
remote: Counting objects: 100% (18/18), done.
remote: Compressing objects: 100% (15/15), done.
remote: Total 18 (delta 3), reused 0 (delta 0)
Unpacking objects: 100% (18/18), done.

bash> cd ChronalCoords2.jl/
bash> julia --project
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.1.0 (2019-01-21)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

(Dojo) pkg> instantiate
  Updating registry at `~/.julia/registries/General`
  Updating git-repo `https://github.com/JuliaRegistries/General.git`
 Resolving package versions...
  Updating `Project.toml`
  [...]
  Updating `Manifest.toml`
  [...]
  
julia> exit()
```

## Test

### using Pkg

```
bash> cd ChronalCoord2.jl/
bash> julia --project
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.1.0 (2019-01-21)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

(Dojo) pkg> test
   Testing Dojo
 Resolving package versions...
Test Summary: | Pass  Total
Dojo          |   10     10
   Testing Dojo tests passed 
   
julia> exit()
```

### Manually

```
bash> cd ChronalCoord2.jl/test
bash> ./runtests.jl 
Test Summary: | Pass  Total
Dojo          |   10     10
```
