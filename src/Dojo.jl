module Dojo
export part1, part2

using Lazy

toInt(x) = parse(Int, x)

function readInput(fname)
    map(eachline(fname)) do l
        split(l, ",") .|> toInt |> Tuple
    end
end

manhattan(a, b) = (b.-a) .|> abs |> sum

function closest(points, p1)
    TIE = length(points) + 1

    iBest = TIE
    dBest = typemax(Int)

    for i in eachindex(points)
        p2 = points[i]
        dist = manhattan(p1, p2)
        if dist < dBest
            iBest, dBest = i, dist
        elseif dist == dBest
            iBest = TIE
        end
    end

    iBest
end

function bbox(points)
    x1, x2 = extrema(p[1] for p in points)
    y1, y2 = extrema(p[2] for p in points)
    (x1,x2, y1, y2)
end

ifilter(f, itr) = (x for x in itr if f(x))

function part1(fname)
    points = readInput(fname)
    x1, x2, y1, y2 = bbox(points)

    N = length(points)
    area =   [0    for _ in 1:N+1]
    finite = [true for _ in 1:N+1]

    foreach(Iterators.product(x1:x2, y1:y2)) do p
        x, y = p
        i = closest(points, p)
        area[i] += 1

        if x in (x1, x2)  || y in (y1, y2)
            finite[i] = false
        end
    end

    best = @>> zip(area, finite)  ifilter(x->x[2])  maximum
    best[1]
end

function part2(fname, N)
    points = readInput(fname)
    x1, x2, y1, y2 = bbox(points)

    # # more readable (IMHO) but slightly sub-optimal performancewise:
    # count(Iterators.product(x1:x2, y1:y2)) do p1
    #     sum(manhattan(p1, p2) for p2 in points) < N
    # end

    # optimal implementation for performance (for loops would be OK too)
    count(Iterators.product(x1:x2, y1:y2)) do p1
        mapfoldl(p2->manhattan(p1,p2), +, points) < N
    end
end

end # module
