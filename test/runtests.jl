#!/bin/bash
#=
exec julia --color=yes runtests.jl
=#

using Dojo
using Dojo: readInput, manhattan, closest, bbox
using Test
using Jive

POINTS = [(1, 1),
          (1, 6),
          (8, 3),
          (3, 4),
          (5, 5),
          (8, 9)]

@testset "Dojo" begin
    @test readInput("test.in") == POINTS
    @test manhattan(POINTS[3], POINTS[5]) == 5

    @testset "closest" begin
        @test closest(POINTS, [4,1]) == 1
        @test closest(POINTS, [5,1]) == 7
        @test closest(POINTS, [6,1]) == 3
    end

    @test bbox(POINTS) == (1,8,1,9)

    @testset "part1" begin
        @test part1("test.in") == 17
        @test part1("complete.in") == 3223
    end

    @testset "part2" begin
        @test part2("test.in", 32) == 16
        @test part2("complete.in", 10_000) == 40495
    end
end

@skip @itest begin
    @profile part1("complete.in")
    Profile.clear()
    @profile part1("complete.in")
end


# Local Variables:
# mode: julia
# End:
